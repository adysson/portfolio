$(document).ready(function() {
  $('body#galleries div#main-content img').not(':first').remove();
  $('a#arrow_right').css('visibility', 'visible');

  var image = $('body#galleries div#main-content img');

  // list next image
  $('#arrow_right').click(function() {
    var nextImage = setNextImage(image);
    console.log("nextImage: " + nextImage);

    if (gallery_images.length > nextImage) {
      image.attr('src', gallery_images[nextImage].url);
      image.attr('data-order', nextImage);
    }

    if (gallery_images.length - 1 == nextImage) {
      $(this).css('visibility', 'hidden');
    }

    if (nextImage > 0) {
      $('#arrow_left').css('visibility', 'visible');
    }
  });

  // list previous image
  $('#arrow_left').click(function() {
    var previousImage = setPreviousImage(image);
    console.log("previousImage: " + previousImage);

    if (previousImage >= 0) {
      image.attr('src', gallery_images[previousImage].url);
      image.attr('data-order', previousImage);
    }

    if (previousImage > 0) {
    }
    else if (previousImage == 0) {
      $(this).css('visibility', 'hidden');
    }

    if (previousImage < gallery_images.length) {
      $('#arrow_right').css('visibility', 'visible');
    }
  });

  // keyboard triggers
  // left
  $(document).keypress(function(e) {
    if (e.key == 'Left') {
      $('#arrow_left').click();
      console.log('#arrow_left clicked');
    }
  });

  // right
  $(document).keypress(function(e) {
    if (e.key == 'Right') {
      $('#arrow_right').click();
      console.log('#arrow_right clicked');
    }
  });

  function setNextImage(image) {
    return parseInt(image.attr('data-order')) + 1;
  }

  function setPreviousImage(image) {
    return parseInt(image.attr('data-order')) - 1;
  }
});