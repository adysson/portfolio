class HomeController < ApplicationController
  skip_before_action :authorize

  def show
    @image = Image.where('show_on_homepage', true).sample
  end
end
