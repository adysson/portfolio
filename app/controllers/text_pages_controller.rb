class TextPagesController < ApplicationController
  skip_before_action :authorize, only: :show
  before_action :set_text_page, only: [:show, :edit, :update, :destroy]
  before_action :set_title, only: :show

  # GET /text_pages
  # GET /text_pages.json
  def index
    @text_pages = TextPage.all
  end

  # GET /text_pages/1
  # GET /text_pages/1.json
  def show
  end

  # GET /text_pages/new
  def new
    @text_page = TextPage.new
  end

  # GET /text_pages/1/edit
  def edit
  end

  # POST /text_pages
  # POST /text_pages.json
  def create
    @text_page = TextPage.new(text_page_params)

    respond_to do |format|
      if @text_page.save
        format.html { redirect_to @text_page, notice: 'Text page was successfully created.' }
        format.json { render :show, status: :created, location: @text_page }
      else
        format.html { render :new }
        format.json { render json: @text_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /text_pages/1
  # PATCH/PUT /text_pages/1.json
  def update
    respond_to do |format|
      if @text_page.update(text_page_params)
        format.html { redirect_to @text_page, notice: 'Text page was successfully updated.' }
        format.json { render :show, status: :ok, location: @text_page }
      else
        format.html { render :edit }
        format.json { render json: @text_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /text_pages/1
  # DELETE /text_pages/1.json
  def destroy
    @text_page.destroy
    respond_to do |format|
      format.html { redirect_to text_pages_url, notice: 'Text page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_text_page
      if current_user.blank?
        @text_page = TextPage.where('active = ?', true)
      else
        @text_page = TextPage
      end

      if params[:url].present?
        @text_page = @text_page.find_by_url(params[:url])
      else params[:id] && current_user.blank?
        @text_page = @text_page.find(params[:id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def text_page_params
      params.require(:text_page)
        .permit(:title, :body, :active, :show_in_menu, :url, :priority)
    end

    def set_title
      @title = "#{@text_page.title} | #{super}"
    end
end
