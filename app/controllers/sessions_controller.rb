class SessionsController < ApplicationController
  skip_before_action :authorize, except: [:destroy]
  def new
    redirect_to root_url, notice: 'already logged in' if current_user.present?
    @user = User.new
  end

  def create
    user = User.find_by_email(params[:email])

    if user.present? && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url, notice: "you're logged in"
    else
      flash.alert = 'bad email/password'
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: 'logged out, bye'
  end
end
