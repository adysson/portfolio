class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_main_menu
  before_action :set_title
  before_action :current_user
  before_action :authorize

  protected

  def set_title
    @title = "adam vasicek photographs"
  end

  private

  def set_main_menu
    @main_menu = {}

    @main_menu[:galleries] = []
    Gallery.by_priority.each do |gallery|
      @main_menu[:galleries] << { title: gallery.title, url: gallery_url(gallery) }
    end

    @main_menu[:text_pages] = []
    TextPage.where('show_in_menu = ?', true).order(:priority).each do |text_page|
      @main_menu[:text_pages] << {
        title: text_page.title, url: specific_text_page_url(text_page.url)
      }
    end

    @main_menu
  end

  def current_user
    @current_user = @current_user ||= User.find(session[:user_id]) if session[:user_id].present?
  end
  helper_method :current_user

  def authorize
    redirect_to root_url, notice: "please log in" if current_user.blank?
  end
end
