json.array!(@text_pages) do |text_page|
  json.extract! text_page, :id, :title, :text, :active, :show_in_menu
  json.url text_page_url(text_page, format: :json)
end
