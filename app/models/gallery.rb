class Gallery < ActiveRecord::Base
  has_many :images

  validates_presence_of :title
  validates_presence_of :url
  validates_uniqueness_of :url

  def images_to_json
    list =  []
    images.order(:priority, :id).each do |image|
      item = "title: \"#{image.title}\", "
      item += "description: \"#{image.description}\", "
      item += "url: \"#{image.image.url(:large)}\""
      list << "{ " + item + " }"
    end
    list.join(",")
  end

  def to_param
    url
  end

  def self.by_priority
    order("priority")
  end
end
