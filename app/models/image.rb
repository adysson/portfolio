class Image < ActiveRecord::Base
  belongs_to :gallery

  has_attached_file :image,
    styles: { large: '800x500>', thumb: '200x200'},
    storage: :dropbox,
    dropbox_credentials: Rails.root.join('config/dropbox.yml'),
    dropbox_visibility: 'public',
    url: '/Public/websites/portfolio/:id_:basename_:style.:extension',
    path: '/Public/websites/portfolio/:id_:basename_:style.:extension'

  validates_attachment_content_type :image, { content_type: ['image/jpeg', 'image/png'] }
  validates_attachment_file_name :image, matches: [/pngZ/, /jpe?g\Z/]
  validates_attachment_presence :image

  def title_and_description
    if title.present? && description.present?
      "#{title}: #{description}"
    elsif title.present?
      title
    elsif description.present?
      description
    else
      ''
    end
  end
end
