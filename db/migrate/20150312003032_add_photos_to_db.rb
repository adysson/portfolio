class AddPhotosToDb < ActiveRecord::Migration
  def up
    Gallery.create(id: 1, title: 'western road', url: 'western-road')
    Gallery.create(id: 2, title: 'portraits', url: 'portraits')
    Image.create(id: 1, gallery_id: 1, image_file_name: 'volny_soubor_01.jpg', image_content_type: 'image/jpeg', image_file_size: 489593, image_updated_at: '2015-03-11 19:12:29 UTC', active: true, show_on_homepage: true, priority: 1)
    Image.create(id: 2, gallery_id: 1, image_file_name: 'volny_soubor_02.jpg', image_content_type: 'image/jpeg', image_file_size: 497879, image_updated_at: '2015-03-11 19:14:20 UTC', active: true, show_on_homepage: false, priority: 2)
    Image.create(id: 3, gallery_id: 1, image_file_name: 'volny_soubor_03.jpg', image_content_type: 'image/jpeg', image_file_size: 402090, image_updated_at: '2015-03-11 19:41:18 UTC', active: true, show_on_homepage: false, priority: 3)
    Image.create(id: 4, gallery_id: 1, image_file_name: 'volny_soubor_04.jpg', image_content_type: 'image/jpeg', image_file_size: 472409, image_updated_at: '2015-03-11 19:43:20 UTC', active: true, show_on_homepage: false, priority: 4)
    Image.create(id: 5, gallery_id: 1, image_file_name: 'volny_soubor_05.jpg', image_content_type: 'image/jpeg', image_file_size: 517315, image_updated_at: '2015-03-11 19:45:32 UTC', active: true, show_on_homepage: true, priority: 5)
    Image.create(id: 6, gallery_id: 1, image_file_name: 'volny_soubor_06.jpg', image_content_type: 'image/jpeg', image_file_size: 365955, image_updated_at: '2015-03-11 19:46:37 UTC', active: true, show_on_homepage: false, priority: 6)
    Image.create(id: 7, gallery_id: 1, image_file_name: 'volny_soubor_07.jpg', image_content_type: 'image/jpeg', image_file_size: 524008, image_updated_at: '2015-03-11 19:48:59 UTC', active: true, show_on_homepage: false, priority: 7)
    Image.create(id: 8, gallery_id: 1, image_file_name: 'volny_soubor_08.jpg', image_content_type: 'image/jpeg', image_file_size: 422435, image_updated_at: '2015-03-11 19:50:10 UTC', active: true, show_on_homepage: false, priority: 8)
    Image.create(id: 9, gallery_id: 1, image_file_name: 'volny_soubor_09.jpg', image_content_type: 'image/jpeg', image_file_size: 487054, image_updated_at: '2015-03-11 19:51:02 UTC', active: true, show_on_homepage: false, priority: 9)
    Image.create(id: 10, gallery_id: 1, image_file_name: 'volny_soubor_10.jpg', image_content_type: 'image/jpeg', image_file_size: 370336, image_updated_at: '2015-03-11 19:51:45 UTC', active: true, show_on_homepage: false, priority: 10)
    Image.create(id: 11, gallery_id: 1, image_file_name: 'volny_soubor_11.jpg', image_content_type: 'image/jpeg', image_file_size: 481176, image_updated_at: '2015-03-11 19:54:08 UTC', active: true, show_on_homepage: false, priority: 11)
    Image.create(id: 12, gallery_id: 1, image_file_name: 'volny_soubor_12.jpg', image_content_type: 'image/jpeg', image_file_size: 354479, image_updated_at: '2015-03-11 19:54:46 UTC', active: true, show_on_homepage: false, priority: 12)
    Image.create(id: 13, gallery_id: 1, image_file_name: 'volny_soubor_13.jpg', image_content_type: 'image/jpeg', image_file_size: 377573, image_updated_at: '2015-03-11 19:55:51 UTC', active: true, show_on_homepage: false, priority: 13)
    Image.create(id: 14, gallery_id: 1, image_file_name: 'volny_soubor_14.jpg', image_content_type: 'image/jpeg', image_file_size: 279486, image_updated_at: '2015-03-11 19:56:29 UTC', active: true, show_on_homepage: false, priority: 14)
    Image.create(id: 15, gallery_id: 1, image_file_name: 'volny_soubor_15.jpg', image_content_type: 'image/jpeg', image_file_size: 455497, image_updated_at: '2015-03-11 19:57:00 UTC', active: true, show_on_homepage: false, priority: 15)
    Image.create(id: 16, gallery_id: 2, image_file_name: 'pod_kozichem.jpg', image_content_type: 'image/jpeg', image_file_size: 250741, image_updated_at: '2015-03-11 19:58:11 UTC', active: true, show_on_homepage: true, priority: 1)
    Image.create(id: 17, gallery_id: 2, image_file_name: 'u_stodoly.jpg', image_content_type: 'image/jpeg', image_file_size: 406593, image_updated_at: '2015-03-11 19:58:43 UTC', active: true, show_on_homepage: false, priority: 2)
  end

  def down
    Image.delete_all
    Gallery.delete_all
  end
end
