class AddActiveToImages < ActiveRecord::Migration
  def change
    add_column :images, :active, :boolean, default: true
  end
end
