class AddUrlToGalleries < ActiveRecord::Migration
  def change
    add_index :galleries, :url, unique: true
  end
end
