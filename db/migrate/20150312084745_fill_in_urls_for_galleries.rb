class FillInUrlsForGalleries < ActiveRecord::Migration
  def change
    Gallery.find_by_title('western road').update(url: 'western-road')
    Gallery.find_by_title('portraits').update(url: 'portraits')
  end
end
