class AddShowOnHomepageToImages < ActiveRecord::Migration
  def change
    add_column :images, :show_on_homepage, :boolean, default: false
  end
end
