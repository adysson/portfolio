class AddPriorityToGalleries < ActiveRecord::Migration
  def change
    add_column :galleries, :priority, :integer
  end
end
