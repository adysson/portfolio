class CreateTextPages < ActiveRecord::Migration
  def change
    create_table :text_pages do |t|
      t.string :title
      t.string :url
      t.text :body
      t.boolean :active
      t.boolean :show_in_menu
      t.integer :priority

      t.timestamps
    end
  end
end
